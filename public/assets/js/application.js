
if (typeof (app) === 'undefined') { var app = {}; }

jQuery(function($) 
{
    /*
    |----------------------------------------------------------------
    |  Return hash from current url
    |----------------------------------------------------------------
    */
    app.getHash = function() {
        return document.location.hash;
    };

    $(document).ready(function()
    {      
        /*
        |----------------------------------------------------------------
        |  Handle hash and tab activity
        |----------------------------------------------------------------
        */  
        var $page_container_link = $('#page-container a');

        if (app.getHash() === '') {
            $page_container_link.eq(0).parent('li').addClass('active');
            $('#page-details').addClass('active');
        } else {
            $(app.getHash()).addClass('active');
            $('a[href='+app.getHash()+']').parent('li').addClass('active');
        }

        $page_container_link.click(function(e) {
            e.preventDefault();
            $(this).tab('show');            
        });        

    });
    
    /*
    |----------------------------------------------------------------
    |  Make slug string from title 
    |----------------------------------------------------------------
    */
    $('input#title').slugify($('#slug'));

    /*
    |----------------------------------------------------------------
    |  Resolve modal confirmation when deleting a page
    |----------------------------------------------------------------
    */
    $('button.delete').on('click', function(e) 
    {
        e.preventDefault();
        var form = $(this).parents('form');
        $('#delete_page').modal();
        $('#delete_page').find('a.trigger').on('click', function(){
            form.submit();
        });
    });

});
