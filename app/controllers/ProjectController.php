<?php 

class ProjectController extends BaseController
{

    public function index() {
        $data['projects'] = Project::all();
        return View::make('backend.project.index', $data);
    }

    public function create() {
        return View::make('backend.project.create');
    }

    public function store() {
        Project::create(Input::all());
        Notification::info('Project '.Input::get('name').' is been added');
        return Redirect::route('project.index');
    }

    public function edit($id) {
        $data = array(
            'project' => Project::find($id),
            'project_images' => ProjectImage::all()
        );
        return View::make('backend.project.edit', $data);
    }

    public function update() {
        return;
    }

    public function uploadImage() {

        $data = array();

        $file = Input::file('screen');
        $directory = public_path() . '/uploads';
        $filename =  $file->getClientOriginalName();

        $upload_success = $file->move($directory, $filename); 

        if ($upload_success) {

            $project_image = new ProjectImage();
            $project_image->filename = $filename;
            $project_image->project_id =  Input::get('project-id');
            $project_image->save();

            $data['data']['image'] = $filename;
            $data['data']['uploaded'] = 1;
            return Response::json($data);

        } else {

            $data['data']['uploaded'] = 0;
            return Response::json($data);
        }
                
    }
}