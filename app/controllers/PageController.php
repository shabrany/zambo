<?php

class PageController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data = array('pages' => Page::all());
		return View::Make('backend.page.list', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::Make('backend.page.create', array('msg' => 'Dit is een test'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array('title' => 'required' );

		$data = Input::all();

		$validator = Validator::make($data, $rules);

		if ($validator->fails())
	    {
	        return Redirect::back()->withInput()->withErrors($validator);
	    }

		Page::create($data);

		Notification::info('Page '.$data['title'].' is been added');

		return Redirect::to(route('admin.page.index'));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data = array('page' => Page::find($id));
		return View::make('backend.page.edit')->with($data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$post_data = Input::all();
		$page = Page::find($id);
		$page->fill($post_data);
		$page->save();
		Notification::success('Changes have been saved for this page');
		return Redirect::route('admin.page.edit', $page->id);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$page = Page::find($id);
		$page->delete();
		Notification::info('Page is been deleted');
		return Redirect::route('admin.page.index');
	}

}