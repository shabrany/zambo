<?php 

/**
 * Authentication Controller. This is class is required 
 * to authenticate an user to the system
 * 
 */
class AuthController extends BaseController
{

    /**
     * Show login form to the user 
     * 
     * @return View
     */
    public function showLogin() {

        if (Auth::check()) 
            return Redirect::to('admin');

        return View::make('auth.login');
    }


    /**
     * Authenticate user 
     * 
     * @return Redirect
     */
    public function authenticateUser() {

        $userdata = array(
            'username' => Input::get('username'),
            'password' => Input::get('password'),
            'is_active' => 1
        );

        $validator = $this->getLoginValidator($userdata);

        // when credentials are missing
        if ($validator->fails()) {
            return Redirect::back()
                ->withInput()
                ->withErrors($validator);
        }

        // When login fails
        if (!Auth::attempt($userdata)) {
            return Redirect::back()
                ->withInput()
                ->withErrors(array('password' => ['Bad credentials']));      
        }
        
        return Redirect::to('admin/login');
    }


    /**
     * Logout the user from the application 
     * 
     * @return Redirect
     */
    public function logoutUser() {
        Auth::logout();
        return Redirect::to('admin/login');
    }


    /**
     * Validate the user credentials and return
     * a Validator object
     * 
     * @param  array $userdata
     * @return Validator
     */
    private function getLoginValidator($userdata) {

        $rules = array( 
            'username' => 'required',
            'password' => 'required'
        );

        return Validator::make($userdata, $rules);
    }

}