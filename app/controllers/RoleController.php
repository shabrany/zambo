<?php 

class RoleController extends BaseController
{
    /**
     * List all roles
     * 
     * @return Response
     */
    public function index() {
        $roles = Role::all();
        $data = array( 'roles' => $roles);
        return View::make('backend.user.role.index', $data);
    }

    /**
     * Show form to add role
     * 
     * @return Response
     */
    public function create() {

        $data = array(
            'permissions' => Permission::all()
        );
        return View::make('backend.user.role.create', $data);
    }

    /**
     * Validate and store new role
     * 
     * @return Response
     */
    public function store() {
        $validator = Validator::make(Input::all(), array(
            'name' => 'required'
        ));

        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator);
        }

        $role = new Role;
        $role->name = Input::get('name');
        $role->save();

        if (Input::has('permission')) {
            $role->assignPermissions(Input::get('permission'));
        }

        return Redirect::route('role.index')->with('message', 'Role added');
    }

    /**
     * Populate form to edit role
     * 
     * @param  int $id
     * @return Response
     */
    public function edit($id) {
        $role = Role::find($id);

        $data = array(
            'role' => $role,
            'permissions' => Permission::all()
        );
        return View::make('backend.user.role.edit', $data);
    }

    /**
     * Validate and update role
     * 
     * @param  int $id
     * @return Response
     */
    public function update($id) {

        $role = Role::find($id);

        $validator = Validator::make(Input::all(), array(
            'name' => 'required'
        ));

        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator);
        }

        # for all removed permissions
        $permissions = array();

        $role->name = Input::get('name');
        $role->save();
        
        if (Input::has('permission')) {
            $permissions = Input::get('permission');
        } 

        $role->assignPermissions($permissions);

        Notification::info('Role has been updated');

        return Redirect::route('role.edit', $role->id);

        // echo "<pre>";
        // print_r(Input::all());
        // print_r($role->permissions()->getRelatedIds());
    }

    /**
     * Delete a rol item
     * 
     * @param  int $id
     * @return Response
     */
    public function delete($id) {

    }
}