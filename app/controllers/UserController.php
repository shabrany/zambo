<?php

class UserController extends BaseController {

    /**
     * Display all users
     * new line 
     * 
     * @return View
     */
    public function index() 
    {
        $data = array( 'users' => User::all());
        return View::make('backend.user.index', $data);
    }

    /**
     * Display user form
     * 
     * @return View
     */
    public function create() 
    {        
        $roles = Role::lists('name', 'id');

        return View::make('backend.user.create', array('roles' => $roles));             
    }

    /**
     * Validate an store a user in the DB
     * 
     * @return Response
     */
    public function store()
    {        
        $rules = array(
            'firstname' => 'required',
            'lastname' => 'required',
            'username' => 'required|min:3',
            'email' => 'required|email|unique:users'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::back()
                ->withInput()
                ->withErrors($validator);
        }

        $user = new User;
        $user->firstname = Input::get('firstname');
        $user->lastname = Input::get('lastname');
        $user->username = Input::get('username');
        $user->email = Input::get('email');
        $user->role_id = Input::get('role');
        
        $user->save();

        Notification::info('User has been added');

        return Redirect::route('user.index');

        // echo "<pre>";print_r(Input::all());
    }

    /**
     * Show user details 
     * 
     * @param  integer $id 
     * @return View      
     */
    public function edit($id) 
    {
        $user = User::find($id);
        $roles = Role::lists('name', 'id');
        $permissions = Permission::all();

        $data = array(
            'user' => $user,
            'permissions' => $permissions,
            'roles' => $roles
        );

        return View::make('backend.user.details', $data);
    }

    public function update($id) 
    {
        $user = User::find($id);
        $role_changed = ($user->role_id != Input::get('role'));
        $user_permissions = array();

        # Assign new role 
        if ($role_changed) {

            # remove all custom permissions 
            if (count($user->permissions)) {
                $user->removePermissions();
            }

            # assign new role
            $user->role_id = Input::get('role');
        }

        # Collect custom permissions if set
        if (Input::has('user_permissions')) {            
            $user_permissions = Input::get('user_permissions');
        }

        # Check if user permissions has changed
        $permissions_changed = ($user_permissions != $user->permissions()->getRelatedIds());

        # set permissions 
        if (!$role_changed && $permissions_changed) {
            $user->setPermissions($user_permissions);
        }
        
        $user->firstname = Input::get('firstname');
        $user->lastname = Input::get('lastname');
        $user->username = Input::get('username');
        $user->email = Input::get('email');        
        $user->is_active = Input::get('is_active');        
        $user->save();

        Notification::info('User has been modified');

        return Redirect::route('user.edit', $user->id);
    }



    public function postDelete() 
    {
        echo "delete user";
    }
}