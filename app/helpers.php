<?php 


if ( ! function_exists('display_status'))
{
    /**
     * HTML helper to display status
     * 
     * @param  int $status 
     * @return string        
     */
    function display_status($status)
    {
        if ($status == 1)
        {
            return '<span class="label label-success">active</span>';
        }
        return '<span class="label label-danger">inactive</span>';
    }
}


if ( ! function_exists('display_fullname'))
{
    /**
     * Display user fullname
     * 
     * @param  User $user 
     * @return string
     */
    function display_fullname($user) 
    {
        return $user->firstname . ' ' . $user->lastname;
    }
}


if ( ! function_exists('display_error'))
{
    /**
     * Html helper to display errors
     * 
     * @param  MessageBag $errors 
     * @param  string $field
     * @return string
     */
    function display_error($errors, $field) 
    {
        if ($errors->has($field)) 
            return '<span>' . $errors->first($field) . '</span>';
    }
}