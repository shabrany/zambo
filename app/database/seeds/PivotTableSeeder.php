<?php 

class PivotTableSeeder extends Seeder 
{
    public function run()
    {
        $this->seedUserPermissions();
        $this->seedUserRoles();
    }

    /**
     * Seed pivot table user_permissions
     */
    public function seedUserPermissions() 
    {
        DB::table('users_permissions')->truncate();

        $admin = DB::table('users')->where('username', 'shabrany')->first();

        $permissions = DB::table('permissions')->get();

        // Seed pivot table
        foreach ($permissions as $value) {
            DB::table('users_permissions')->insert(array(
                'user_id' => $admin->id,
                'permission_id' => $value->id
            ));
        }
    }

    /**
     * [seedUserRoles description]
     */
    public function seedUserRoles() 
    {
        $admin = DB::table('users')->where('username', 'shabrany')->first();

        $adminRole = DB::table('roles')->where('name', 'admin')->first();

        DB::table('users')
            ->where('id', $admin->id)
            ->update(array('role_id' => $adminRole->id));
    }
}