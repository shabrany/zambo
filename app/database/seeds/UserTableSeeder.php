<?php  


class UserTableSeeder extends Seeder {

    public function run()
    { 
        DB::table('users')->truncate();

        User::create(array(
            'firstname' => 'Jorge',
            'lastname'  => 'Fernandez',
            'email' => 'yoyo.armani@gmail.com',
            'username' => 'shabrany',
            'password' => Hash::make('monyer'),
            'is_active' => 1
        ));
    }

}