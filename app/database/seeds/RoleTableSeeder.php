<?php  


class RoleTableSeeder extends Seeder {

    public function run()
    {

        DB::table('roles')->truncate();

        DB::table('roles')->insert(array(
            array(
                'name' => 'admin',
                'description' => 'Administrator'
            ),
            array(
                'name' => 'user',
                'description' => 'User of system. No any special permissions.'
            ),
        ));
    }

}