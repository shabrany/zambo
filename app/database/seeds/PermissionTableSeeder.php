<?php 

class PermissionTableSeeder extends Seeder {

    public function run()
    {
        $this->seedPermissions();
    }


    /**
     * Seed table permissions
     */
    public function seedPermissions() 
    {
        DB::table('permissions')->truncate();

        DB::table('permissions')->insert( array(
            
            array(
                'name' => 'create_page',
                'description'  => '',
            ),
            array(
                'name' => 'edit_page',
                'description'  => '',
            ),
            array(
                'name' => 'delete_page',
                'description'  => '',
            ),
            array(
                'name' => 'create_user',
                'description'  => '',
            ),
            array(
                'name' => 'edit_user',
                'description'  => '',
            ),                  
            array(
                'name' => 'delete_user',
                'description'  => '',
            ),
        ));
    }

    


}