<?php 


class Page extends Eloquent {

    protected $fillable = ['title', 'slug', 'content', 'published', 'meta_keywords', 'meta_desc'];

}
