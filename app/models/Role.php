<?php 


class Role extends Eloquent 
{
    protected $table = 'roles';

    public $timestamps = false;

    public function permissions() {
        return $this->belongsToMany('Permission', 'roles_permissions');
    }

    public function user()
    {
        return $this->hasOne('User');
    }

    public function assignPermissions($permissions) {
        $this->permissions()->sync($permissions);
    }

    public function hasPermission($name) {
        $permissions = array_fetch($this->permissions->toArray(), 'name');
        return in_array($name, $permissions);
    }

}