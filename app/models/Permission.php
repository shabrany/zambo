<?php 


class Permission extends Eloquent 
{
    protected $table = 'permissions';

    public $timestamps = false;

    public function roles() 
    {
        return $this->belongsToMany('Role', 'roles_permissions');
    }

    public function users()
    {
       return $this->belongsToMany('Users', 'users_permissions');
    }
}