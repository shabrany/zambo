<?php 

class Project extends Eloquent
{
    
    protected $fillable = ['name', 'description'];
}