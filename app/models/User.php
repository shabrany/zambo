<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

	
	public function role()
	{
		return $this->belongsTo('Role');
	}

	public function permissions()
	{
		return $this->belongsToMany('Permission', 'users_permissions');
	}

	public function getAllPermissions() 
	{
		$role_permissions = $this->role->permissions->toArray();
		$user_permissions = $this->permissions->toArray();
		$all = array_merge($role_permissions, $user_permissions);
		return $all;
	}

	public function can($name)
	{
		$permissions = array_fetch($this->getAllPermissions(), 'name');
		return in_array($name, $permissions);
	}

	public function removePermissions() 
	{
		return DB::table('users_permissions')->where('user_id', $this->id)->delete();
	}

	public function setPermissions($permissions)
	{
		$this->permissions()->sync($permissions);
	}

}