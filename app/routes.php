<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::controller('password', 'RemindersController');


/**
 * -------------------------------------------------------
 *  Admin routes
 *  ------------------------------------------------------
 */
Route::group(array('prefix' => 'admin'), function() 
{

    # Authentication routes
    Route::get('login', array('as' => 'login.get', 'uses' => 'AuthController@showLogin')); 
    Route::post('login', array('as' => 'login.post', 'uses' => 'AuthController@authenticateUser'));
    Route::get('logout', array('as' => 'logout.get', 'uses' => 'AuthController@logoutUser'));

    # Routes where authentication is needed
    Route::group(array('before' => 'auth'), function() 
    {
        Route::get('/', 'PageController@index');    
    
        # Page management routes
        Route::resource('page', 'PageController');

        # User management routes
        Route::get('user', array('as' => 'user.index', 'uses' => 'UserController@index'));
        Route::get('user/edit/{id}',  array('as' => 'user.edit', 'uses' => 'UserController@edit'));
        Route::post('user/edit/{id}',  array('as' => 'user.update', 'uses' => 'UserController@update'));
        Route::get('user/create',  array('as' => 'user.create', 'uses' => 'UserController@create'));
        Route::post('user/store',  array('as' => 'user.store', 'uses' => 'UserController@store'));
        
        # Roles management routes
        Route::get('roles', array(
            'as' => 'role.index',
            'uses' => 'RoleController@index',
        ));
        Route::get('roles/create', array(
            'as' => 'role.create',
            'uses' => 'RoleController@create',
        ));
        Route::post('roles/create', array(
            'as' => 'role.store',
            'uses' => 'RoleController@store',
        ));
        Route::get('roles/edit/{id}', array(
            'as' => 'role.edit',
            'uses' => 'RoleController@edit',
        ));
        Route::post('roles/edit/{id}', array(
            'as' => 'role.update',
            'uses' => 'RoleController@update',
        ));

        # Routes project management
        Route::get('projects', array('as' => 'project.index', 'uses' => 'ProjectController@index'));
        Route::get('projects/create', array('as' => 'project.create', 'uses' => 'ProjectController@create'));
        Route::post('projects/store', array('as' => 'project.store', 'uses' => 'ProjectController@store'));
        Route::get('projects/edit/{id}', array('as' => 'project.edit', 'uses' => 'ProjectController@edit'));
        Route::post('projects/update/{id}', array('as' => 'project.update', 'uses' => 'ProjectController@update'));
        Route::post('projects/upload', array('as' => 'project.upload', 'uses' => 'ProjectController@uploadImage'));
    });       

});




/**
 * -------------------------------------------------------
 *  Frontend routes
 *  ------------------------------------------------------
 */
Route::get('/{slug}', function($slug)
{
    $page = Page::where('slug',$slug)->first();

    if (!$page) App::abort(404, $slug.' was not found. Next time better');

    return View::make('frontend.content', array('page' => $page));
});


Route::get('/', function() 
{
    return View::make('hello');
});


App::missing(function($exception)
{
    return Response::view('404', array(), 404);
});


