<?php 

return array(

    'close'         => 'Close',
    'save'          => 'Save',
    'confirm'       => 'Confirm',
    'cancel'        => 'Cancel',

    'page_del'      => 'Delete page',
    'page_add'      => 'Add page',
    'page_edit'     => 'Edit page',

    'password_forgotten' => 'Password forgotten?',

    'login' => array(
        'title' => 'Login',
        'uname' => 'Username',
        'email' => 'E-mailaddress',
        'pass' => 'Password',
        'submit' => 'Login'
    )
);