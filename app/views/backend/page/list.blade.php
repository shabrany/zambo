@extends('backend.base')

@section('content')

<div class="block-wrapper">

    <div class="block-head">
        <h4>Pages</h4>
        <div class="btn-group pull-right">
            <a href="{{ URL::Route('admin.page.create') }}" class="btn"><i class="icon icon-plus"></i></a>
        </div>
    </div>

    <div class="block-body">

        <table class="table table-hover my-table">
            
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Slug</th>
                    <th>Created at</th>
                    <th>Published</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($pages as $page)
                <tr>
                    <td>{{ $page->id }}</td>
                    <td><a href="{{ route('admin.page.edit', $page->id) }}">{{ $page->title }}</a></td>
                    <td>{{ $page->slug }}</a></td>
                    <td>{{ $page->created_at }}</td>
                    <td>{{ display_status($page->published) }}</td>
                    <td class="text-right">
                        <a href="{{ route('admin.page.edit', $page->id) }}" class="btn btn-warning"><i class="icon-edit  icon-white"></i></a>                    
                        {{ Form::open(['action' => ['PageController@destroy', $page->id], 'method' => 'DELETE', 'class' => 'delete']) }}
                        <button class="btn btn-danger delete"
                                {{-- href="{{ route('admin.page.edit', $page->id) }}" --}}
                                ><i class="icon-trash icon-white"></i></button>
                        {{ Form::close() }}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

</div>

@stop

@section('javascripts')
    @parent
    @include('backend._partial.notification')    
@stop