@extends('backend.base')

@section('content')

<div class="block-wrapper">
    
    <div class="block-head">
        <h4>ADD PAGE</h4>
        <div class="btn-group pull-right">
            <a href="{{ route('admin.page.index') }}" class="btn"><b>cancel</b></a>
        </div>
    </div>
    
    <div class="block-body">
        
        {{ Form::open(array('route' => 'admin.page.store', 'class' => 'form')) }}

        <ul class="nav nav-tabs" id="page-container">
            <li><a href="#page-details">Page details</a></li>
            <li><a href="#page-content">Content</a></li>
            <li><a href="#page-meta">Meta data</a></li>
        </ul>

        <div class="tab-content">

            <!-- #page-details -->

            <div class="tab-pane" id="page-details">

                <div class="contol-group">
                    {{ Form::label('title','Title', array('class' => 'control-label')) }}
                    <div class="controls">
                        {{ Form::input('text', 'title') }}
                    </div>           
                </div> <hr>

                <div class="contol-group">
                    {{ Form::label('slug','Slug', array('class' => 'control-label')) }}
                    <div class="controls">
                        <div class="input-prepend">
                            <span class="add-on">{{ Request::root() }}/</span>
                            {{ Form::input('text', 'slug') }}
                        </div>                        
                    </div>           
                </div> <hr>

                <div class="contol-group">
                    {{ Form::label('published','Published', array('class' => 'control-label')) }}
                    <div class="controls">
                        <label class="radio inline">
                            {{ Form::radio('published', '1'); }} Yes
                        </label>
                        <label class="radio inline">
                            {{ Form::radio('published', '0', true); }} No
                        </label>                        
                    </div>           
                </div>

            </div><!-- end #page-details -->

            <!-- #page-content -->

            <div class="tab-pane" id="page-content">
                <div class="contol-group">
                    <label class="control-label">Content</label>
                    <div class="controls">
                        {{ Form::textarea('content', '', array('class' => 'content')) }}
                    </div>           
                </div>
            </div><!-- end #page-content -->

            <!-- #page-meta -->

            <div class="tab-pane" id="page-meta">

                <div class="contol-group">
                    {{ Form::label('meta_keywords','Meta keywords', array('class' => 'control-label')) }}
                    <div class="controls">
                        {{ Form::input('text', 'meta_keywords') }}                                
                    </div>           
                </div> <hr>

                <div class="contol-group">
                    {{ Form::label('meta_desc','Meta description', array('class' => 'control-label')) }}
                    <div class="controls">
                        {{ Form::textarea('meta_desc', '') }}                                
                    </div>           
                </div> <hr>
                
            </div>

        </div>
        
        <div class="form-actions">
            <input type="submit" name="save" value="Pagina toevoegen" class="btn btn-primary">
        </div>

        {{ Form::close() }}
        
    </div>
    
</div>
    
@stop


@section('javascripts')

    @parent

    <script type="text/javascript" src="/assets/js/jquery.slugify.js"></script>

    @include('backend._partial.tinymce_script')

@stop