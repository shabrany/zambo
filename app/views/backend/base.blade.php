<!DOCTYPE html>
<html>
<head>
    <title>ZamboCMS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="/assets/css/bootstrap.min.css" rel="stylesheet" media="screen" />
    <link rel="stylesheet" type="text/css" href="/assets/css/alertify_themes/alertify.core.css" />
    <link rel="stylesheet" type="text/css" href="/assets/css/alertify_themes/alertify.default.css" />
    <link href="/assets/css/admin.css" rel="stylesheet" media="screen" />

</head>
<body>
    
    <div class="container">
        
        <div id="header">
            <div class="hdrl"></div>
            <div class="hdrr"></div>
            <h4><a href="#">Zambo 4.0</a></h4>
            <ul id="nav">
                <li><a href="{{ URL::to('admin/page') }}">Pages</a></li>
                <li class="my-dropdown">
                    <a href="{{ URL::to('admin/user') }}">Users</a>
                    <ul class="submenu">
                        <li><a href="{{ route('role.index') }}">Roles</a></li>
                        <li><a href="">Permissions</a></li>
                    </ul>
                </li>
                <li><a href="{{ route('project.index') }}">Projects</a></li>
                <li class="lastchild"><a href="#">Settings</a></li>
            </ul>
            <p class="user">
                Welcome, 
                <a href="">{{ Auth::user()->firstname }}</a> | 
                <a href="{{ URL::to('admin/logout') }}">Logout</a>
            </p> 
        </div>

        <div id="content">
            @yield('content')
        </div>

        <div id="footer">
            <p class="pull-left">monocode.nl</p>
            <p class="pull-right">Powered by Monocode</p>
        </div>  
        
    </div>

    @include('backend._partial.confirmation')

    @section('javascripts') 
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    {{ HTML::script('assets/js/jquery-1.11.2.min.js') }}
    <script type="text/javascript" src="/assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/assets/js/jquery.slugify.js"></script>
    <script type="text/javascript" src="/assets/js/application.js"></script>
    
    @show

</body>
</html>