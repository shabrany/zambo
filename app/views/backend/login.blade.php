<!DOCTYPE html>
<html>
<head>
    <title>ZamboCMS / login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="/assets/css/bootstrap.min.css" rel="stylesheet" media="screen" />
    <link href="/assets/css/admin.css" rel="stylesheet" media="screen" />

</head>
<body>
    
    <div class="login-outer">                 
        <div class="login-inner">

            @yield('content')                


        </div>
    </div>

    @section('javascripts') 
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <script type="text/javascript" src="/assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/assets/js/application.js"></script>
    
    @show

</body>
</html>