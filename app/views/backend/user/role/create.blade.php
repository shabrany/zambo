@extends('backend.base')

@section('content')
<div class="block-wrapper">

    <div class="block-head">
        <h4>Create role</h4>
        <div class="btn-group pull-right">
            <a href="{{ route('role.create') }}" class="btn"><i class="icon icon-plus"></i></a>
        </div>
    </div>

    <div class="block-body"> 

    {{ Form::open(array('route' => 'role.store', 'class' => 'form')) }}

        <div class="control-group">
            <label class="control-label">Role name</label>
            <div class="controls">
                {{ Form::text('name') }}
                {{ display_error($errors, 'name')}}
            </div>
        </div>

        <div class="control-group">
            <h4>Assign permisions to role</h4>
            <div class="permissions">
                @foreach ($permissions as $key => $permission)
                    <label class="checkbox">
                        {{ Form::checkbox('permission[]', $permission->id) }}    
                        {{ str_replace('_', ' ', ucfirst($permission->name)) }}                    
                    </label>                                       
                @endforeach
            </div>
        </div>

        <div class="form-controls">
            <button class="btn btn-primary" type="submit">{{ trans('labels.save') }}</button>
        </div>

    {{ Form::close() }}

    </div>


</div>
@stop