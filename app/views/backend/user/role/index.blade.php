@extends('backend.base')

@section('content')
<div class="block-wrapper">

    <div class="block-head">
        <h4>Roles</h4>
        <div class="btn-group pull-right">
            <a href="{{ route('role.create') }}" class="btn"><i class="icon icon-plus"></i></a>
        </div>
    </div>

    <div class="block-body">        

        <table class="table table-hover my-table">
            
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Permissions</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($roles as $role)
                <tr>
                    <td>{{ $role->id }}</td>
                    <td>{{ $role->name }}</td>
                    <td> 
                        @foreach ($role->permissions as $permission)
                            [{{ $permission->name }}]
                        @endforeach
                    </td>
                    <td class="text-right">
                        <a href="{{ route('role.edit', $role->id) }}" class="btn btn-small btn-warning">
                            <i class="icon-white icon-edit"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
                
            </tbody>
        </table>
    </div>

</div>
   
@stop