@extends('backend.base')

@section('content')
<div class="block-wrapper">

    <div class="block-head">
        <h4>Modify role</h4>
        <div class="btn-group pull-right">
            <a href="{{ route('role.index') }}" class="btn">list roles</a>
        </div>
    </div>

    <div class="block-body"> 

    {{ Form::model($role, array('route' => array('role.update', $role->id))) }}

        <div class="control-group">
            <label class="control-label">Name</label>
            <div class="controls">
                {{ Form::text('name') }}
                {{ display_error($errors, 'name')}}
            </div>
        </div>

        <div class="control-group">
            <h4>Assign permisions to role</h4>
            <div class="permissions">
                @foreach ($permissions as $key => $permission)
                    <label class="checkbox">
                        <?php $checked = $role->hasPermission($permission->name) ?>
                        {{ Form::checkbox('permission[]', $permission->id, $checked) }}    
                        {{ str_replace('_', ' ', ucfirst($permission->name)) }}                    
                    </label>                                       
                @endforeach
            </div>
        </div>

        <div class="form-controls">
            <button class="btn btn-primary" type="submit">{{ trans('labels.save') }}</button>
        </div>

    {{ Form::close() }}

    </div>


</div>
@stop

@section('javascripts')
    @parent
    @include('backend._partial.notification')    
@stop