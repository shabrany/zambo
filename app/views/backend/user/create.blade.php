@extends('backend.base')

@section('content')

<div class="block-wrapper">
    
    <div class="block-head">
        <h4>ADD USER</h4>
        <div class="btn-group pull-right">
            <a href="{{ route('admin.page.index') }}" class="btn"><b>cancel</b></a>
        </div>
    </div>    

    <div class="block-body">      

        {{ Form::open(array('route' => 'user.store', 'class' => 'form-horizontal')) }}

        <div class="control-group">
            {{ Form::label('firstname','', array('class' => 'control-label')) }}
            <div class="controls">
                {{ Form::input('text', 'firstname') }}
                {{ display_error($errors, 'firstname') }}
            </div>           
        </div> 

        <div class="control-group">
            {{ Form::label('lastname','', array('class' => 'control-label')) }}
            <div class="controls">
                {{ Form::input('text', 'lastname') }}
                {{ display_error($errors, 'lastname') }}
            </div>           
        </div>

        <div class="control-group">
            {{ Form::label('username','', array('class' => 'control-label')) }}
            <div class="controls">
                {{ Form::input('text', 'username') }}
                {{ display_error($errors, 'username') }}
            </div>           
        </div>

        <div class="control-group">
            {{ Form::label('email','', array('class' => 'control-label')) }}
            <div class="controls">
                {{ Form::input('text', 'email') }}
                {{ display_error($errors, 'email') }}
            </div>           
        </div>

        <div class="control-group">
            {{ Form::label('role','Roles', array('class' => 'control-label')) }}
            <div class="controls">
                {{ Form::select('role', $roles) }}
                {{ display_error($errors, 'role') }}
            </div>           
        </div>

        <div class="form-actions">
            <button type="submit" class="btn btn-primary">{{ trans('labels.save') }}</button>           
        </div>

        {{ Form::close() }}
    </div>
    
    
    
@stop