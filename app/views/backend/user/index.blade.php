@extends('backend.base')

@section('content')

<div class="block-wrapper">

    <div class="block-head">
        <h4>Users</h4>
        <div class="btn-group pull-right">
            <a href="{{ route('user.create') }}" class="btn"><i class="icon icon-plus"></i></a>
        </div>
    </div>

    <div class="block-body">

        <table class="table table-hover my-table">
            
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Created at</th>
                    <th>Active</th>
                    <th>Role</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                
                @foreach ($users as $user)                                  
                <tr>
                    <td>{{ $user->id }}</td>
                    <td><a href="{{ route('user.edit', $user->id) }}">{{ display_fullname($user) }}</a></td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->created_at }}</td>
                    <td>{{ display_status($user->is_active) }}</td>
                    <td>{{ $user->role->name }}</td>
                    <td class="text-right">
                        <a href="" class="btn btn-warning"><i class="icon-edit  icon-white"></i></a>                                           
                        <a href="" class="btn btn-danger delete"><i class="icon-trash icon-white"></i></a>                        
                    </td>
                </tr>

                @endforeach
            </tbody>
        </table>
    </div>

</div>
@stop