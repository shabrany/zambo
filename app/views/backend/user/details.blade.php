@extends('backend.base')

@section('content')

<div class="block-wrapper">
    
    <div class="block-head">
        <h4>EDIT USER</h4>
        <div class="btn-group pull-right">
            <a href="{{ route('user.index') }}" class="btn"><b>cancel</b></a>
        </div>
    </div>    

    <div class="block-body">      

        {{ Form::model($user, array('route' => array('user.edit', $user->id), 'class' => 'form-horizontal')) }}

        <div class="row-fluid">
            
            <div class="span6">
                
                <h4>Personal info</h4>
                <div class="control-group">
                    {{ Form::label('firstname','', array('class' => 'control-label')) }}
                    <div class="controls">
                        {{ Form::input('text', 'firstname') }}
                        {{ display_error($errors, 'firstname') }}
                    </div>           
                </div> 

                <div class="control-group">
                    {{ Form::label('lastname','', array('class' => 'control-label')) }}
                    <div class="controls">
                        {{ Form::input('text', 'lastname') }}
                        {{ display_error($errors, 'lastname') }}
                    </div>           
                </div>

                <div class="control-group">
                    {{ Form::label('username','', array('class' => 'control-label')) }}
                    <div class="controls">
                        {{ Form::input('text', 'username') }}
                        {{ display_error($errors, 'username') }}
                    </div>           
                </div>

                <div class="control-group">
                    {{ Form::label('email','', array('class' => 'control-label')) }}
                    <div class="controls">
                        {{ Form::input('text', 'email') }}
                        {{ display_error($errors, 'email') }}
                    </div>           
                </div>

                <div class="control-group">
                    {{ Form::label('active', '', array('class' => 'control-label')) }}
                    <div class="controls">
                        {{ Form::checkbox('is_active', 1, $user->is_active) }}
                        {{ display_error($errors, 'role') }}
                    </div>           
                </div>

                <div class="control-group">
                    {{ Form::label('role','Roles', array('class' => 'control-label')) }}
                    <div class="controls">
                        {{ Form::select('role', $roles, $user->role->id) }}
                        {{ display_error($errors, 'role') }}
                    </div>           
                </div>
                

            </div>

            <div class="span6">
                
                <h4>Role permissions ({{ $user->role->name }})</h4>
                <div class="control-group">
                    @foreach ($user->role->permissions as $permission)
                        <label class="checkbox">
                            <input type="checkbox" checked disabled>
                            {{ $permission->name }}
                        </label>
                    @endforeach
                </div>

                <h4>Extra permissions</h4>
                <div class="control-group">
                    @foreach ($permissions as $permission)
                        
                        @if (!$user->role->hasPermission($permission->name))
                            <label class="checkbox">    
                            <?php $checked = $user->can($permission->name) ?> 
                            {{ Form::checkbox('user_permissions[]', $permission->id, $checked) }}               
                            {{ $permission->name }}
                            </label>
                        @endif
                        
                    @endforeach
                </div>

            </div>

        </div>

        <div class="form-actions">
            <button type="submit" class="btn btn-primary">{{ trans('labels.save') }}</button>           
        </div>

        {{ Form::close() }}
    </div>
    
    
    
@stop

@section('javascripts')
    @parent   
    @include('backend._partial.notification')
@stop