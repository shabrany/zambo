
<script type="text/javascript" src="/assets/js/alertify.min.js"></script>
<script type="text/javascript">
    {{ Notification::showError(' alertify.error(":message");') }} 
    {{ Notification::showInfo(' alertify.log(":message");') }} 
    {{ Notification::showSuccess(' alertify.success(":message");') }}
</script>