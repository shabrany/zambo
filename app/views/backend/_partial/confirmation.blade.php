<div id="delete_page" class="modal hide fade">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>  
        <h3>{{ trans('labels.page_del') }}</h3>
    </div>
    <div class="modal-body">
        <p>{{ trans('messages.page_msg_del') }}</p>
    </div>
    <div class="modal-footer">
        <a class="btn" data-dismiss="modal" aria-hidden="true">{{ trans('labels.cancel') }}</a>
        <a href="#" class="btn btn-primary trigger">{{ trans('labels.confirm') }}</a>
    </div>
</div>
