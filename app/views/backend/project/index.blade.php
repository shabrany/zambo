@extends('backend.base')

@section('content')
<div class="block-wrapper">

    <div class="block-head">
        <h4>Projects</h4>
        <div class="btn-group pull-right">
            <a href="{{ route('project.create') }}" class="btn"><i class="icon icon-plus"></i></a>
        </div>
    </div>

    <div class="block-body">        

        @if (count($projects))
        <table class="table table-hover my-table">
            
            <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($projects as $project)
                <tr>
                    <td>{{ $project->id }}</td>
                    <td>{{ $project->name }}</td>
                    <td class="text-right">
                        <a href="{{ route('project.edit', $project->id) }}" class="btn btn-small btn-warning">
                            <i class="icon-white icon-edit"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
                
            </tbody>
        </table>
        @else 

        <h3>No data found</h3>
        @endif

    </div>

</div>
   
@stop

@section('javascripts')
    @parent
    @include('backend._partial.notification')    
@stop