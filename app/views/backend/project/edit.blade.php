@extends('backend.base')

@section('content')
<div class="block-wrapper">

    <div class="block-head">
        <h4>Projects</h4>
        <div class="btn-group pull-right">
            <a href="{{ route('project.create') }}" class="btn"><i class="icon icon-plus"></i></a>
        </div>
    </div>

    <div class="block-body">     

        <div class="row-fluid">
            
            <div class="span4">
                
                {{ Form::model($project, array('class' => 'form', 'route' => array('project.store', $project->id))) }}

                <div class="control-group">
                    <label class="control-label">Project name:</label>
                    <div class="controls">
                        {{ Form::text('name') }}
                        {{ display_error($errors, 'name')}}
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Description:</label>
                    <div class="controls">
                        {{ Form::textarea('description') }}
                        {{ display_error($errors, 'description')}}
                    </div>
                </div>

                <div class="form-controls">
                    <button class="btn btn-primary" type="submit">{{ trans('labels.save') }}</button>
                </div>

                {{ Form::close() }}

            </div>

            <div class="span8">
            {{ public_path() }}

                {{ Form::open(array('id' => 'screen-dropzone', 'class' => 'dropzone dz-clickable', 'route' => 'project.upload', 'files' => true)) }}
                {{ Form::hidden('project-id', $project->id)}}
                <div class="dz-message">
                    <h4>Drag Photos to Upload</h4>
                    <span>Or click to browse</span>
                </div>
                {{ Form::close() }}
            </div>

        </div>

                

    </div>

</div>
   
@stop

@section('javascripts')
    
    @parent
    {{ HTML::script('assets/js/dropzone.js') }}

    <script type="text/javascript">


    Dropzone.options.screenDropzone = {
        paramName: 'screen',
        init: function() {
            this.on('addedfile', function(file) {
                //console.log(file);
            });
        },
        success: function(file, data) {
            console.log(data);
        }
    };

    

    </script>
@stop