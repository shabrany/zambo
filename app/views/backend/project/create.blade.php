@extends('backend.base')

@section('content')
<div class="block-wrapper">

    <div class="block-head">
        <h4>Create project</h4>
        <div class="btn-group pull-right">
            <a href="{{ route('project.create') }}" class="btn"><i class="icon icon-plus"></i></a>
        </div>
    </div>

    <div class="block-body"> 

    {{ Form::open(array('route' => 'project.store', 'class' => 'form')) }}

        <div class="control-group">
            <label class="control-label">Project name:</label>
            <div class="controls">
                {{ Form::text('name') }}
                {{ display_error($errors, 'name')}}
            </div>
        </div>

        <div class="control-group">
            <label class="control-label">Description:</label>
            <div class="controls">
                {{ Form::textarea('description') }}
                {{ display_error($errors, 'description')}}
            </div>
        </div>

        <div class="form-controls">
            <button class="btn btn-primary" type="submit">{{ trans('labels.save') }}</button>
        </div>

    {{ Form::close() }}

    </div>


</div>
@stop