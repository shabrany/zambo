@extends('backend.login')

@section('content')
    
    {{ Form::open(['class' => 'form']) }}
    <fieldset>
        <legend>Login</legend>
        <div class="control-group">
            <label for="username" class="control-label">{{ trans('labels.login.uname') }}</label>
            <div class="controls">
                {{ Form::text('username', '', array('class' => 'input-block-level')) }}                         
            </div>
        </div>
    
        <div class="control-group">
            <label for="password" class="control-label">{{ trans('labels.login.pass') }}</label>
            <div class="controls">
                {{ Form::password('password', array('class' => 'input-block-level')) }}
            </div>
        </div>

        <div class="control-group">
            <a href="{{ URL::to('password/remind') }}">{{ trans('labels.password_forgotten') }}</a>
        </div>

        <div class="control-group">
            <div class="controls">
                <button type="submit" class="btn btn-primary">{{ trans('labels.login.submit') }}</button>
            </div>
        </div>
        
    </fieldset>
    {{ Form::close() }}

    @if ($errors->has())
        <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
        </ul>
    @endif

@stop