@extends('backend.login')

@section('content')

    @if (Session::has('error'))
        
    {{ Session::get('error') }}
    @endif


    <form action="{{ action('RemindersController@postRemind') }}" method="POST" class="form">

        <fieldset>
            <legend>Password forgotten</legend>

            <div class="control-group">
                <label>{{ trans('labels.login.email') }}</label>
                <div class="controls">
                    {{ Form::text('email', Input::old('email'), array('class' => 'input-block-level')) }}
                </div>                
            </div>
            
            <div class="control-group">
                <input type="submit" value="Send Reminder" class="btn">
            </div>           
            

        </fieldset>
    </form>

@stop